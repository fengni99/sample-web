/**
 * Copyright (c) 2016 Baozun All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Baozun.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Baozun.
 *
 * BAOZUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. BAOZUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.discovery.darchrow.redis;

import redis.clients.jedis.Jedis;

/**
 * <h1>redis学习</h1>
 * <p>需先在本地部署redis服务端</p1>
 * @author dongliang.ma
 * @date 2016年10月31日下午5:45:23
 */
public class RedisJava {
	
	public static void main(String[] args) {
		//连接本地的 Redis 服务
	    @SuppressWarnings("resource")
		Jedis jedis = new Jedis("127.0.0.1");
	    //查看服务是否运行
	    try {
			System.out.println("Server is running: "+jedis.ping());
			System.out.println("Connection to server sucessfully");
		    //字符串
		    /*String redisKey ="runoobkey";
		    jedis.set(redisKey, "Redis tutorial");
		    System.out.println("Stored string in redis :" + jedis.get(redisKey));
		    //list
		    String redisLkey="tutorial-list";
		    jedis.lpush(redisLkey, "Redis");
		    jedis.lpush(redisLkey, "Mongodb");
		    jedis.lpush(redisLkey, "Mysql");
		    List<String> list = jedis.lrange(redisLkey, 0, 5);
		    list.stream()
		    	.forEach(str -> System.out.println(str));
		    
		    Set<String> set = jedis.keys("*");
		    set.stream().forEach(str -> System.out.println(str));*/
		    
		    /*jedis.zadd("top", 50, "a");
		    jedis.zadd("top", 100, "b");
		    jedis.zadd("top", 99, "c");
		    jedis.zadd("top", 10, "d");
		    jedis.zadd("top", 66, "e");
		    
		    Set<String> zrevrange = jedis.zrevrange("top", 0, 2);
		    zrevrange.stream().forEach(str -> System.out.println(str));*/
			
			String redisLkey="tutorial-list222";
		    jedis.lpush(redisLkey, "Redis");
		    jedis.lpush(redisLkey, "Mongodb");
		    jedis.lpush(redisLkey, "Mysql");
			
		    String lpop = jedis.lpop(redisLkey);
			System.out.println(lpop);
			
		    
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Fail to connect server");
		}
	    
	}
}
