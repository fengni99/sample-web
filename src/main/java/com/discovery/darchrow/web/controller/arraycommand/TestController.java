/**
 * Copyright (c) 2010 Jumbomart All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Jumbomart.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Jumbo.
 *
 * JUMBOMART MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. JUMBOMART SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.discovery.darchrow.web.controller.arraycommand;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.discovery.darchrow.entity.User;
import com.discovery.darchrow.tools.jsonlib.JsonUtil;
import com.discovery.darchrow.web.bind.ArrayCommand;

@Controller
@RequestMapping("/test1")
public class TestController {
	
	private static final Logger log = LoggerFactory.getLogger(TestController.class);
	
	
	@RequestMapping(value = "/test.htm", method = RequestMethod.GET)
	public String test(HttpServletRequest request, Model model) {
		
		return "test";
	}
	
	@RequestMapping(value = "/saveSimple.json", method = RequestMethod.POST)
	@ResponseBody
	public String saveSimple(
			@ArrayCommand(name="p1", dataBind =true) String[] p1,
			HttpServletRequest request, Model model) {
		
		return "success";
	}
	
	@RequestMapping(value = "/saveEntity.json", method = RequestMethod.POST)
	@ResponseBody
	public String saveEntity(
			@ArrayCommand(name="user") User[] user,
			HttpServletRequest request, Model model) {
//		log.debug("user:{}",Json);
		return "success";
	}
	
	@RequestMapping(value = "/saveMEntity.json", method = RequestMethod.POST)
	@ResponseBody
	public String saveMEntity(
			@ArrayCommand(name="user") User[] user,
			HttpServletRequest request, Model model) {
		log.debug("user:{}",JsonUtil.format(user));
		return "success";
	}
	
	@RequestMapping(value = "/saveM2Entity.json", method = RequestMethod.POST)
	@ResponseBody
	public String saveM2Entity(
			@ArrayCommand(name="user") User[] user,
			HttpServletRequest request, Model model) {
		log.debug("user:{}",JsonUtil.format(user));
		return "success";
	}
	
}
