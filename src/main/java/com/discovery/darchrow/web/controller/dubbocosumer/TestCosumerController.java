package com.discovery.darchrow.web.controller.dubbocosumer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.discovery.darchrow.mybatis.api.DemoUserService;
import com.discovery.darchrow.mybatis.model.User;
import com.discovery.darchrow.tools.jsonlib.JsonUtil;

@Controller
public class TestCosumerController {

//	private static final Logger log = LoggerFactory.getLogger(TestCosumerController.class);
	
	@Autowired
	private DemoUserService demoService;
	
	@RequestMapping(value = "/testquery.htm", method = RequestMethod.GET)
	public String testquery(HttpServletRequest request, Model model) {
		
	    User user = demoService.findById(1);
	    
		model.addAttribute("user", JsonUtil.format(user));
		
		return "consumer/testquery";
	}
}
